## Contributing

First off, thank you for considering contributing to TRIAS-py.

### Where do I go from here?

If you've noticed a bug or have a feature request, [make one][new issue]! It's
generally best if you get confirmation of your bug or approval for your feature
request this way before starting to code.

### Fork & create a branch

If this is something you think you can fix, then [fork this repo] and create
a branch with a descriptive name.

A good branch name would be (where issue #325 is the ticket you're working on):

```sh
git checkout -b 325-add-support-for-stop-requests
```

### Implement your fix or feature

At this point, you're ready to make your changes! Feel free to ask for help;
everyone is a beginner at first :)

### Get the style right

Your patch should follow the same conventions & pass the same code quality
checks as the rest of the project. `pylint` will give you feedback in
this regard. When you pushed your fix the continuos integration pipeline should
give you some hints.

### Make a Merge Request

At this point, you should switch back to your master branch and make sure it's
up-to-date with TRIAS-py master branch:

```sh
git remote add upstream git@gitlab.com:motis-datagon/trias-py.git
git checkout master
git pull upstream master
```

Then update your feature branch from your local copy of master, and push it!

```sh
git checkout 325-add-support-for-stop-requests
git rebase master
git push --set-upstream origin 325-add-support-for-stop-requests
```

Finally, go to Gitlab and [make a Merge Request] :D

Gitlab CI will run our linter. We care about quality, so your PR won't be merged until 
all tests pass.

### Keeping your Merge Request updated

If a maintainer asks you to "rebase" your PR, they're saying that a lot of code
has changed, and that you need to update your branch, so it's easier to merge.

To learn more about rebasing in Git, there are a lot of [good][git rebasing]
[resources][interactive rebase] but here's the suggested workflow:

```sh
git checkout 325-add-support-for-stop-requests
git pull --rebase upstream master
git push --force-with-lease 325-add-support-for-stop-requests
```

### Merging an MR (maintainers only)

An MR can only be merged into master by a maintainer if:

* It is passing CI.
* It has been approved by at least one maintainer. If it was a maintainer who
  opened the MR, one extra approval is needed.
* It has no requested changes.
* It is up-to-date with current master.

Any maintainer is allowed to merge an MR if all of these conditions are
met.

[new issue]: https://gitlab.com/motis-datagon/trias-py/-/issues/new
[fork this repo]: https://gitlab.com/motis-datagon/trias-py/-/forks/new
[make a Merge Request]: https://gitlab.com/motis-datagon/trias-py/-/merge_requests/new
[git rebasing]: http://git-scm.com/book/en/Git-Branching-Rebasing
[interactive rebase]: https://help.github.com/en/github/using-git/about-git-rebase
