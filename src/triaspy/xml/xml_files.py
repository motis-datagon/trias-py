"""
XML templates to build the requests
"""

TRIAS_TRIP_REQUEST = """<?xml version="1.0" encoding="UTF-8"?>
<Trias version="$TRIAS_VERSION" xmlns="http://www.vdv.de/trias" xmlns:siri="http://www.siri.org.uk/siri"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" \
xsi:schemaLocation="https://raw.githubusercontent.com/VDVde/TRIAS/v$TRIAS_VERSION/Trias.xsd">
    <ServiceRequest>
        <siri:RequestTimestamp>$REQUEST_TIMESTAMP</siri:RequestTimestamp>
        <siri:RequestorRef>$TOKEN</siri:RequestorRef>
        <RequestPayload>
            <TripRequest>
                $ORIGIN
                $VIA
                $DESTINATION
                <Params>
                    <IncludeTurnDescription>true</IncludeTurnDescription>
                    <IncludeTrackSections>true</IncludeTrackSections>
                    <IncludeLegProjection>true</IncludeLegProjection>
                    <IncludeIntermediateStops>true</IncludeIntermediateStops>
                    <IncludeFares>$INCLUDE_FARES</IncludeFares>
                </Params>
            </TripRequest>
        </RequestPayload>
    </ServiceRequest>
</Trias>"""
VIA = """<Via>
    <ViaPoint>
        <StopPointRef>$VIA</StopPointRef>
    </ViaPoint>
</Via>"""
TRIAS_STOP_EVENT_REQUEST = """<?xml version="1.0" encoding="UTF-8" ?>
<Trias version="$TRIAS_VERSION" xmlns="http://www.vdv.de/trias" xmlns:siri="http://www.siri.org.uk/siri" \
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" \
xsi:schemaLocation="https://raw.githubusercontent.com/VDVde/TRIAS/v$TRIAS_VERSION/Trias.xsd">
    <ServiceRequest>
        <siri:RequestTimestamp>$REQUEST_TIMESTAMP</siri:RequestTimestamp>
        <siri:RequestorRef>$TOKEN</siri:RequestorRef>
        <RequestPayload>
            <StopEventRequest>
                $LOCATION
                <Params>
                    <NumberOfResults>$MAX_RESULTS</NumberOfResults>
                    <StopEventType>departure</StopEventType>
                    <IncludePreviousCalls>false</IncludePreviousCalls>
                    <IncludeOnwardCalls>false</IncludeOnwardCalls>
                    <IncludeRealtimeData>true</IncludeRealtimeData>
                </Params>
            </StopEventRequest>
        </RequestPayload>
    </ServiceRequest>
</Trias>"""


TRIAS_LOCATION_INFORMATION_REQUEST = """<?xml version="1.0" encoding="utf-8" ?>
<Trias xmlns="http://www.vdv.de/trias" xmlns:siri="http://www.siri.org.uk/siri" \
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="$TRIAS_VERSION">
    <ServiceRequest>
        <siri:RequestTimestamp>$REQUEST_TIMESTAMP</siri:RequestTimestamp>
        <siri:RequestorRef>$TOKEN</siri:RequestorRef>
            <RequestPayload>
                <LocationInformationRequest>
                    $LOCATION_INPUT
                    $RESTRICTIONS
                </LocationInformationRequest>
            </RequestPayload>
    </ServiceRequest>
</Trias>
"""
