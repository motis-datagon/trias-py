"""
main module providing the TRIASClient
"""
import json
from importlib import resources
from typing import Optional

from triaspy.handler.trias_location_information_handler import TRIASLocationInformationHandler
from triaspy.handler.trias_stop_event_handler import TRIASStopEventHandler
from triaspy.model import options
from triaspy import configurations
from triaspy.handler.trias_trip_handler import TRIASTripHandler


def get_client(config_name: str, authorization_key: Optional[str], is_testing_endpoint=False):
    """
    Offers a handy way to get a TRIAS-Client instance
    :param is_testing_endpoint: whether the test-endpoint should be used
    :param authorization_key: key to authorize when needed
    :param config_name: name of the config to load
    :return: a TRIAS-Client instance
    """
    if is_testing_endpoint:
        config_filename = config_name + "-trias-test.json"
    else:
        config_filename = config_name + "-trias.json"
    config_file = resources.files(configurations).joinpath(config_filename).read_text()
    configuration = json.loads(config_file)
    authorization_method = configuration["options"]["authorizationMethod"]
    url = configuration["options"]["endpoint"]
    trias_version = str(configuration["options"]["triasVersion"])
    headers = {"content-type": configuration["options"]["requestContentType"]}
    if authorization_method == "Authorization-Header":
        headers["Authorization"] = authorization_key
        authorization_key = "gateway-api"
    client_options = options.ClientOptions(url, authorization_key, trias_version, headers)
    return TRIASClient(client_options)


class TRIASClient:
    """
    TRIASClient for communicating with a specific TRIAS-API
    """

    def __init__(self, client_options: options.ClientOptions):
        """
        Constructor of the Client
        :param client_options: options to configure the TRIASClient
        """
        if not client_options.requestor_ref:
            client_options.set_requestor_ref("")
        if not client_options.headers:
            client_options.set_headers({})
        self.location_information_handler = \
            TRIASLocationInformationHandler(client_options.url, client_options.requestor_ref,
                                            client_options.headers, client_options.trias_version)
        self.departures_handler = TRIASStopEventHandler(client_options.url, client_options.requestor_ref,
                                                        client_options.headers, client_options.trias_version)
        self.journey_handler = TRIASTripHandler(client_options.url, client_options.requestor_ref,
                                                client_options.headers, client_options.trias_version)

    def get_location_information(self, location_information_options: options.LocationInformationRequestOptions):
        """
        Returns the available departure based on specified options
        :param location_information_options: the options for the location
        :return: the global ID
        """
        return self.location_information_handler.get_location_information(location_information_options)

    def get_stop_events(self, departure_options: options.StopEventRequestOptions):
        """
        Returns the available departure based on specified options
        :param departure_options: the options for which the departure is requested
        :return: the available departures
        """
        return self.departures_handler.get_stop_events(departure_options)

    def get_trips(self, trip_options: options.TripRequestOptions):
        """
        returns the available trips based on specified options
        :param trip_options: the options for which the trip is requested
        :return: the available trips
        """
        return self.journey_handler.get_trips(trip_options)
