"""
Defined classes to model certain parts of the Requests as objects
"""
from typing import Union, Tuple, Optional

from triaspy.trias.common_xml_structures.trias_location_support.location_reference_structure import LocationReference


class LocationContext:
    """
    Specification of a location and the ways in which a user can reach it.
    """

    def __init__(self, location_reference: Union[str, Tuple[str, str]], departure_time: Optional[str] = None,
                 name: str = "Location"):
        self.__location_reference: Union[str, Tuple[str, str]] = location_reference
        self.__departure_time: Optional[str] = departure_time
        self.__name = name

    def get_location_reference(self):
        """
        Return the location reference
        :return: the location reference
        """
        return self.__location_reference

    def get_departure_time(self):
        """
        Return the departure time
        :return: the departure time
        """
        return self.__departure_time

    def get_longitude(self):
        """
        Returns the longitude of the geo-point
        :return: the longitude of the geo-point
        """
        return self.__location_reference[0]

    def get_latitude(self):
        """
        Returns the longitude of the geo-point
        :return: the longitude of the geo-point
        """
        return self.__location_reference[1]

    def get_xml_representation(self):
        """
        Returns the XML-Representation of this LocationContext
        :return: the XML-Representation of this LocationContext
        """
        if isinstance(self.__location_reference, str):
            location_reference = LocationReference(stop_point_reference=self.__location_reference)
        else:
            location_reference = LocationReference(geo_position=self.__location_reference)
        xml_representation = \
            f"""<$NAME>
                    {location_reference.get_xml_representation()}
                    $DEPARTURE_TIME
                </$NAME>"""
        if self.__departure_time is not None:
            xml_representation = xml_representation.replace("$DESTINATION", "<DepArrTime>$TIME</DepArrTime>")
            xml_representation = xml_representation.replace("$TIME", self.__departure_time)
        else:
            xml_representation = xml_representation.replace("$DESTINATION", "")
        xml_representation = xml_representation.replace("$NAME", self.__name)

        return xml_representation
