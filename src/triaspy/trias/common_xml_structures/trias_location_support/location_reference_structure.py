"""
Module for the Location Reference
"""
from typing import Optional, Tuple


class LocationReference:
    """
    Reference to a general location point (stop point, stop, coordinate position, location or POI).
    """
    def __init__(self, stop_point_reference: Optional[str] = None, geo_position: Optional[Tuple[str, str]] = None):
        self.__geo_position: Optional[Tuple[str, str]] = geo_position
        self.__stop_point_reference: Optional[str] = stop_point_reference

    def get_geo_position(self):
        """
        Return the geo position
        :return: the geo position
        """
        return self.__geo_position

    def get_xml_representation(self):
        """
        Returns the XML-Representation of this LocationReference
        :return: the XML-Representation of this LocationReference
        """
        if self.__stop_point_reference is not None:
            location_reference = f"""<StopPointRef>{self.__stop_point_reference}</StopPointRef>"""
        else:
            location_reference = f"""<GeoPosition>
                                        <Longitude>{self.__geo_position[0]}</Longitude>
                                        <Latitude>{self.__geo_position[1]}</Latitude>
                                    </GeoPosition>"""
        xml_representation = \
            f"""<LocationRef>
                    {location_reference}
               </LocationRef>"""
        return xml_representation
