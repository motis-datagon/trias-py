"""
Module for InitialLocationInput
"""
from typing import Tuple, Optional


class InitialLocationInput:
    """
    Initial input which gets resolved to a location
    """
    def __init__(self, location_name: Optional[str] = None, geo_position: Optional[Tuple[str, str]] = None):
        self.__location_name = location_name
        self.__geo_position = geo_position

    def get_geo_position(self):
        """
        Return the geo position
        :return: the geo position
        """
        return self.__geo_position

    def get_xml_representation(self):
        """
        Returns the XML-Representation of this initial location input
        :return: the XML-Representation of this initial location input
        """
        xml_representation = \
            f"""<InitialInput>
                        <LocationName>{self.__location_name}</LocationName>
                </InitialInput>"""
        return xml_representation
