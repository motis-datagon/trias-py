"""
Module for the LocationParameter class
"""
from typing import Optional


class LocationParameter:
    """
    Summarises request parameters which are used in the location information service
    """
    def __init__(self, location_data_filter: Optional[dict] = None, location_policy: Optional[dict] = None):
        self.__location_data_filter = location_data_filter
        self.__location_policy = location_policy

    def get_location_policy(self):
        """
        Return the location policy
        :return: the location policy
        """
        return self.__location_policy

    def get_xml_representation(self):
        """
        Returns the XML-Representation of this location parameters
        :return: the XML-Representation of location parameters
        """
        xml_representation = f"""
        <Restrictions>
            <Type>{self.__location_data_filter["type"]}</Type>
        </Restrictions>"""
        return xml_representation
