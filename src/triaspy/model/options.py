"""
This Module contains informal interfaces for parameter options
"""
from typing import List, Optional, Tuple


class ClientOptions:
    """
    Informal Interface for options that can be given to a client
    """

    def __init__(self, url, requestor_ref, trias_version: str, headers=None):
        if headers is None:
            headers = {}
        self.url = url
        self.requestor_ref = requestor_ref
        self.headers = headers
        self.trias_version = trias_version

    def set_requestor_ref(self, requestor_ref):
        """
        Updates the requestor_ref
        :param requestor_ref: updated requestor_ref
        """
        self.requestor_ref = requestor_ref

    def set_headers(self, headers):
        """
        Updates the __headers
        :param headers: updated __headers
        """
        self.headers = headers


class LocationInformationRequestOptions:
    """
    Informal Interface for options that can be given to a LocationInformationHandler
    """

    def __init__(self, location_name: Optional[str] = None, location_type: Optional[str] = None,
                 stop_point_ref: Optional[str] = None):
        self.__location_name = location_name
        self.__location_type = location_type
        self.__stop_point_ref = stop_point_ref

    def get_location_name(self):
        """
        Returns the location name
        :return: the location name
        """
        return self.__location_name

    def get_location_type(self):
        """
        Returns the location type
        :return: the location type
        """
        return self.__location_type

    def get_stop_point_reference(self):
        """
        Returns the stop point reference
        :return: the stop point reference
        """
        return self.__stop_point_ref


class StopEventRequestOptions:
    """
    Informal Interface for options that can be given to a StopHandler
    """

    def __init__(self, station_id: str, departure_time: str, max_results: int):
        self.__station_id: str = station_id
        self.__time: str = departure_time
        self.__max_results: int = max_results

    def get_station_id(self):
        """
        Returns the station_id
        :return: the station_id
        """
        return self.__station_id

    def get_time(self):
        """
        Returns time-point option
        :return: time-point option
        """
        return self.__time

    def get_max_results(self):
        """
        Return maximum wanted results
        :return: maximum wanted results
        """
        return self.__max_results


class TripRequestOptions:
    """
    Informal Interface for options that can be given to a TripHandler
    """

    def __init__(self, trip_request_options: {}):
        if "departure_time" not in trip_request_options.keys() or "trip_parameter" not in trip_request_options.keys():
            raise ValueError
        self.__departure_time: str = trip_request_options["departure_time"]
        self.__trip_parameter: {} = trip_request_options["trip_parameter"]

        if "via" in trip_request_options.keys():
            self.__via: List[str] = trip_request_options["via"]
        else:
            self.__via: Optional[List[str]] = None
        if "origin_id" in trip_request_options.keys():
            self.__origin_id: str = trip_request_options["origin_id"]
        else:
            self.__origin_id: Optional[str] = None
        if "origin_geo_point" in trip_request_options.keys():
            self.__origin_geo_point: Optional[Tuple[str, str]] = trip_request_options["origin_geo_point"]
        else:
            self.__origin_geo_point: Optional[Tuple[str, str]] = None
        if "destination_id" in trip_request_options.keys():
            self.__destination: str = trip_request_options["destination_id"]
        else:
            self.__destination: Optional[str] = None
        if "destination_geo_point" in trip_request_options.keys():
            self.__destination_geo_point: Optional[Tuple[str, str]] = trip_request_options["destination_geo_point"]
        else:
            self.__destination_geo_point: Optional[Tuple[str, str]] = None

    def get_departure_time(self):
        """
        Returns the departure time of the origin
        :return: returns the departure time
        """
        return self.__departure_time

    def are_fares_included(self):
        """
        Returns whether fares should be included
        :return: whether fares should be included
        """
        return self.__trip_parameter["include_fares"]

    def get_origin_id(self):
        """
        Returns the origin reference which can e.g. be a DHID
        :return: the origin reference of the origin
        """
        return self.__origin_id

    def get_destination_id(self):
        """
        Returns the destination id of the Trip
        :return: the destination id
        """
        return self.__destination

    def get_via(self):
        """
        Returns the via points
        :return: the list of via points
        """
        return self.__via

    def get_origin_geo_point(self):
        """
        Returns the origin geo point
        :return: the origin geo point
        """
        return self.__origin_geo_point

    def get_destination_geo_point(self):
        """
        Returns the origin geo point
        :return: the origin geo point
        """
        return self.__destination_geo_point
