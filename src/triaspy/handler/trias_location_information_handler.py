"""
Handler for TRIAS LocationInformation Endpoints
"""
from triaspy.handler.trias_handler import TRIASHandler
from triaspy.model.options import LocationInformationRequestOptions
from triaspy.trias.common_xml_structures.trias_location_support.location_reference_structure import LocationReference
from triaspy.trias.location_information_service.request_structures.initial_location_input import InitialLocationInput
from triaspy.trias.location_information_service.request_structures.location_parameter import LocationParameter
from triaspy.xml.xml_files import TRIAS_LOCATION_INFORMATION_REQUEST
from triaspy.request import request


class TRIASLocationInformationHandler(TRIASHandler):
    """
    Handler for TRIAS LocationInformation Endpoints
    """

    def get_location_information_request(self, location_information_options: LocationInformationRequestOptions):
        """
        Creates the LocationInformation Request according to the given options
        :param location_information_options: options on which location information should be retrieved
        :return: Request body for a location information request
        """
        request_body = TRIAS_LOCATION_INFORMATION_REQUEST
        if location_information_options.get_location_name() is not None:
            initial_input = InitialLocationInput(location_name=location_information_options.get_location_name())
            request_body = request_body.replace("$LOCATION_INPUT", initial_input.get_xml_representation())
            restrictions = \
                LocationParameter(location_data_filter={"type": location_information_options.get_location_type()})
            request_body = request_body.replace("$RESTRICTIONS", restrictions.get_xml_representation())
        else:
            location_ref = LocationReference(location_information_options.get_stop_point_reference())
            request_body = request_body.replace("$LOCATION_INPUT", location_ref.get_xml_representation())
            request_body.replace("$RESTRICTIONS", "")
        request_body = request_body.replace("$TRIAS_VERSION", self.get_trias_version())
        request_body = request_body.replace("$TOKEN", self.get_requestor_reference())
        return request_body

    def get_location_information(self, location_information_options: LocationInformationRequestOptions):
        """
        Requests information about a given location according to the given options
        :param location_information_options: options about which location information should be retrieved
        :return: the information retrieved
        """
        request_body = self.get_location_information_request(location_information_options)
        return request(url=self.get_url(), request_body=request_body, headers=self.get_headers())
