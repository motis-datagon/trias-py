"""
Abstract Handler for TRIAS Endpoints
"""
from abc import ABC


class TRIASHandler(ABC):
    """
    Abstract Handler for TRIAS Endpoints
    """

    def __init__(self, url: str, requestor_reference: str, headers: dict, trias_version: str):
        self.__url = url
        self.__requestor_reference = requestor_reference
        self.__headers = headers
        self.__trias_version = trias_version

    def get_url(self):
        """
        Returns the URL of the handler
        """
        return self.__url

    def get_requestor_reference(self):
        """
        Returns the requestor reference of the handler
        """
        return self.__requestor_reference

    def get_headers(self):
        """
        Returns the headers of the handler
        """
        return self.__headers

    def get_trias_version(self):
        """
        Returns the trias version of the handler
        """
        return self.__trias_version
