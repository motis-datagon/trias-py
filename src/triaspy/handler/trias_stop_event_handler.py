"""
Handler for TRIAS Departure Endpoints
"""
from triaspy.handler.trias_handler import TRIASHandler
from triaspy.model.options import StopEventRequestOptions
from triaspy.trias.common_xml_structures.trias_journey_support.location_context_structure import LocationContext
from triaspy.xml.xml_files import TRIAS_STOP_EVENT_REQUEST
from triaspy.request import request


class TRIASStopEventHandler(TRIASHandler):
    """
    Handler for TRIAS Departure Endpoints
    """

    def get_stop_event_request(self, stop_event_options: StopEventRequestOptions):
        """
        Creates the StopEvent Request according to the given options
        :param stop_event_options: options on which stop events should be gotten
        :return: the departures according to the given options
        """
        location = LocationContext(location_reference=stop_event_options.get_station_id(),
                                   departure_time=stop_event_options.get_time())
        request_body = TRIAS_STOP_EVENT_REQUEST
        request_body = request_body.replace("$LOCATION", location.get_xml_representation())
        request_body = request_body.replace("$MAX_RESULTS", str(stop_event_options.get_max_results()))
        request_body = request_body.replace("$TOKEN", self.get_requestor_reference())
        request_body = request_body.replace("$TRIAS_VERSION", self.get_trias_version())
        return request_body

    def get_stop_events(self, stop_event_options: StopEventRequestOptions):
        """
        Requests departures according to the given options
        :param stop_event_options: options on which departures should be gotten
        :return: the departures according to the given options
        """
        request_body = self.get_stop_event_request(stop_event_options)
        return request(url=self.get_url(), request_body=request_body, headers=self.get_headers())
