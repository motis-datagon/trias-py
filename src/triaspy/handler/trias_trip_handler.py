"""
Handler for TRIAS TripRequests
"""
from triaspy.handler.trias_handler import TRIASHandler
from triaspy.model.options import TripRequestOptions
from triaspy.trias.common_xml_structures.trias_journey_support.location_context_structure import LocationContext
from triaspy.xml.xml_files import TRIAS_TRIP_REQUEST, VIA
from triaspy.request import request


class TRIASTripHandler(TRIASHandler):
    """
    Handler for TRIAS TripRequest
    """

    def get_trip_request(self, trip_options: TripRequestOptions):
        """
        Creates a TripRequest according to the given options
        :param trip_options: options on which journeys should get retrieved
        :return: trip request build according to the options
        """
        if trip_options.get_origin_id() is not None:
            origin = LocationContext(location_reference=trip_options.get_origin_id(),
                                     departure_time=trip_options.get_departure_time(), name="Origin")
        else:
            origin = LocationContext(location_reference=trip_options.get_origin_geo_point(),
                                     departure_time=trip_options.get_departure_time(), name="Origin")
        if trip_options.get_destination_id() is not None:
            destination = LocationContext(location_reference=trip_options.get_destination_id(),
                                     departure_time=trip_options.get_departure_time(), name="Destination")
        else:
            destination = LocationContext(location_reference=trip_options.get_destination_geo_point(),
                                     departure_time=trip_options.get_departure_time(), name="Destination")
        request_body = TRIAS_TRIP_REQUEST
        request_body = request_body.replace("$ORIGIN", origin.get_xml_representation())
        request_body = request_body.replace("$DESTINATION", destination.get_xml_representation())
        request_body = request_body.replace("$INCLUDE_FARES", str(trip_options.are_fares_included()).lower())

        if trip_options.get_via() is not None:
            request_body = request_body.replace("$VIA", VIA)
            request_body = request_body.replace("$VIA", trip_options.get_via()[0])
        else:
            request_body = request_body.replace("$VIA", "")
        request_body = request_body.replace("$TOKEN", self.get_requestor_reference())
        request_body = request_body.replace("$TRIAS_VERSION", self.get_trias_version())
        return request_body

    def get_trips(self, trip_options: TripRequestOptions):
        """
        Return the journeys according to the given options
        :param trip_options: options on which journeys should get retrieved
        :return: trips according to the given options
        """
        request_body = self.get_trip_request(trip_options)
        return request(url=self.get_url(), request_body=request_body, headers=self.get_headers())
