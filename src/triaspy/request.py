"""
module to deal with requests and responses
"""
import datetime

import requests


def request(url: str, headers: dict, request_body: str):
    """
    Sends a Request to a specified API

    There is a known issue with the User-Agent header, when it is not set explicitly to the given value
    see https://python.tutorialink.com/post-request-to-trias-api-does-not-work-with-requests/

    :param url: __url where to send the request to
    :param headers: __headers for the request
    :param request_body: body for the request
    :return: the answer of the request
    """
    headers["User-Agent"] = "Python-urllib/3.10"
    request_timestamp = datetime.datetime.now().replace(microsecond=0).isoformat() + "Z"
    request_body = request_body.replace("$REQUEST_TIMESTAMP", request_timestamp)
    return requests.post(url=url, data=request_body, headers=headers, timeout=10000)
