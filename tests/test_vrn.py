"""
Test to get things working with the VRN Endpoint
To make the tests work, the APIKEY for the trias-endpoint should be provided in the environment variable APIKEY
"""
import os

from triaspy import api
from triaspy.model.options import LocationInformationRequestOptions, TripRequestOptions


def test_vrn_location_information_request():
    """
    Sends an example LocationInformationRequest to the VRN trias endpoint
    """
    vrn_client = api.get_client("vrn", os.getenv("APIKEY"), True)
    request_options = LocationInformationRequestOptions("schwetzingen, schloss", "stop")
    location_information = vrn_client.get_location_information(request_options)
    assert location_information.status_code == 200


def test_vrn_trip_request_basic():
    """
    Sends an example TripRequest to the VRN trias endpoint
    Origin and Destination are given as DHID
    """
    vrn_client = api.get_client("vrn", os.getenv("APIKEY"), is_testing_endpoint=True)
    request_options = TripRequestOptions({"origin_id": "de:08226:3505", "departure_time": "2019-11-27T10:00:00",
                                          "destination_id": "de:08221:1146", "trip_parameter": {"include_fares": True}})
    trips = vrn_client.get_trips(request_options)
    assert trips.status_code == 200
