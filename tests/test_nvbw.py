"""
Test to get things working with the bwegt Endpoint
To make the tests work, the APIKEY for the trias-endpoint should be provided in the environment variable APIKEY
"""
import os

from triaspy import api
from triaspy.model.options import StopEventRequestOptions, TripRequestOptions, LocationInformationRequestOptions


def test_nvbw_location_information_request():
    """
    Sends an example LocationInformationRequest to the NVBW trias endpoint
    """
    nvbw_client = api.get_client("bwegt", os.getenv("APIKEY"))
    request_options = LocationInformationRequestOptions(stop_point_ref="de:8326074:6")
    location_information = nvbw_client.get_location_information(request_options)
    assert location_information.status_code == 200


def test_nvbw_stop_request():
    """
    Sends an example StopEventRequest to the bwegt trias endpoint
    """
    bwegt_client = api.get_client("bwegt", os.getenv("APIKEY"))
    request_options = StopEventRequestOptions(station_id="de:06439:11318", departure_time="2022-01-18T09:00:00Z",
                                              max_results=10)
    stop_events = bwegt_client.get_stop_events(request_options)
    assert stop_events.status_code == 200


def test_nvbw_trip_request_basic():
    """
    Sends an example TripRequest to the bwegt trias endpoint
    Origin and Destination are given as DHID
    """
    bwegt_client = api.get_client("bwegt", os.getenv("APIKEY"))
    request_options = TripRequestOptions({"origin_id": "de:06439:18136", "departure_time": "2022-01-18T09:48:00",
                                          "destination_id": "de:08111:6115", "trip_parameter": {"include_fares": True}})
    trips = bwegt_client.get_trips(request_options)
    assert trips.status_code == 200


def test_nvbw_trip_request_complex():
    """
    Sends an example TripRequest to the bwegt trias endpoint
    Origin and Destination are given as Geopoints (longitude/latitude), additionally a via point is given as DHID
    """
    bwegt_client = api.get_client("bwegt", os.getenv("APIKEY"))
    request_options = TripRequestOptions({"departure_time": "2022-01-18T09:49:00",
                                          "trip_parameter": {"include_fares": True},
                                          "origin_geo_point": ("8.30824", "50.24906"),
                                          "destination_geo_point": ("8.32933", "48.42716"), "via": ["de:08111:6115"]})
    trips = bwegt_client.get_trips(request_options)
    assert trips.status_code == 200
