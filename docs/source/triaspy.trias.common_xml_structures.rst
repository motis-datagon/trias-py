triaspy.trias.common\_xml\_structures package
=============================================

.. automodule:: triaspy.trias.common_xml_structures
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   triaspy.trias.common_xml_structures.trias_journey_support
   triaspy.trias.common_xml_structures.trias_location_support
