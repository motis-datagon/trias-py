triaspy.trias.location\_information\_service.request\_structures package
========================================================================

.. automodule:: triaspy.trias.location_information_service.request_structures
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   triaspy.trias.location_information_service.request_structures.initial_location_input
   triaspy.trias.location_information_service.request_structures.location_parameter
