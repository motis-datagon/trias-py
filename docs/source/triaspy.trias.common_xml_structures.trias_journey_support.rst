triaspy.trias.common\_xml\_structures.trias\_journey\_support package
=====================================================================

.. automodule:: triaspy.trias.common_xml_structures.trias_journey_support
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   triaspy.trias.common_xml_structures.trias_journey_support.location_context_structure
