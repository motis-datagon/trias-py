triaspy.model.options module
============================

.. automodule:: triaspy.model.options
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
