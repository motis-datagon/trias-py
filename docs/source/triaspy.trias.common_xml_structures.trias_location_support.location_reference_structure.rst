triaspy.trias.common\_xml\_structures.trias\_location\_support.location\_reference\_structure module
====================================================================================================

.. automodule:: triaspy.trias.common_xml_structures.trias_location_support.location_reference_structure
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
