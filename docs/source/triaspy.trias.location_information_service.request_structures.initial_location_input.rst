triaspy.trias.location\_information\_service.request\_structures.initial\_location\_input module
================================================================================================

.. automodule:: triaspy.trias.location_information_service.request_structures.initial_location_input
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
